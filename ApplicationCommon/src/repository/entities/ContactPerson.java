package repository.entities;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
/*
 * @author Flloyd Dsouza
 * 
 */
@Entity
@Table(name = "CONTACT_PERSON")
@NamedQueries({@NamedQuery(name = ContactPerson.GET_ALL_QUERY_NAME, query = "SELECT c FROM ContactPerson c order by c.contactPersonId desc")})
public class ContactPerson implements Serializable{

	public static final String GET_ALL_QUERY_NAME = "ContactPerson.getAll";
	
    private int contactPersonId;
    private String name;
    private String gender;
    private Date dateOfBirth;
    private String phoneNumber;
    private String email;
    private Address address;
    private Customer customer;

    public ContactPerson() {
    }

    public ContactPerson(int contactPersonId, String name, String gender, Date dateOfBirth, String phoneNumber, String email, Address address, Customer customer) {
        this.contactPersonId = contactPersonId;
        this.name = name;
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.address = address;
        this.customer = customer;
    }

    @Id
    @GeneratedValue
    @Column(name = "contact_person_id")
    public int getContactPersonId() {
        return contactPersonId;
    }

    public void setContactPersonId(int contactPersonId) {
        this.contactPersonId = contactPersonId;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "gender")
    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Column(name = "date_of_birth")
    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    @Column(name = "phone_number")
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Embedded
    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
    
    
    @ManyToOne
    public Customer getCustomer() {
  		return customer;
  	}

  	public void setCustomer(Customer customer) {
  		this.customer = customer;
  	}
    

  	public String dateOfBirthToString() {
  		if (dateOfBirth != null) {
  			DateFormat dateFormat = new SimpleDateFormat("dd-mm-yyyy");  
            String strDate = dateFormat.format(dateOfBirth);
            return strDate;
  		}
  		return "";
  	}
  	
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + this.contactPersonId;
        return hash;
    }

  

	@Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ContactPerson other = (ContactPerson) obj;
        if (this.contactPersonId != other.contactPersonId) {
            return false;
        }
        return true;
    }


    @Override
    public String toString() {
        return "ContactPerson{" +
                "contactPersonId=" + contactPersonId +
                ", name='" + name + '\'' +
                ", gender=" + gender +
                ", dateOfBirth=" + dateOfBirth +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", email='" + email + '\'' +
                ", address=" + address +
                '}';
    }

    
}
