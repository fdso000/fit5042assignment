package controllers;
import java.util.List;

import javax.el.ELContext;
import javax.inject.Named;

import repository.entities.ContactPerson;

import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Flloyd Dsouza
 */
@Named(value = "contactPersonController")
@RequestScoped
public class ContactPersonController {
	
	 private int contactPersonId; 
	 //contactPersonId is the index, not contactPersonId of a contactPerson

	public int getContactPersonId() {
		return contactPersonId;
	}

	public void setContactPersonId(int contactPersonId) {
		this.contactPersonId = contactPersonId;
	}

	private repository.entities.ContactPerson contactPerson;
	
	
	public ContactPersonController() {
		contactPersonId = Integer.valueOf(FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("contactPersonId"));
		
		contactPerson = getContactPerson();
	}
	 
	public repository.entities.ContactPerson getContactPerson(){
		 if (contactPerson == null) {
	           
	            ELContext context
	                    = FacesContext.getCurrentInstance().getELContext();
	            CustomerApplication app
	                    = (CustomerApplication) FacesContext.getCurrentInstance()
	                            .getApplication()
	                            .getELResolver()
	                            .getValue(context, null, "customerApplication");
	           
	            
	           List<ContactPerson> contactPersons = app.getContactPersons(); 
	            
	            for (int i= 0; i < contactPersons.size(); i++) {
	            	if(contactPersons.get(i).getContactPersonId() == contactPersonId) {
	            		return contactPersons.get(i);
	            	}
	            }
	            
	           // return app.getContactPersons().get(--contactPersonId);
	        }
	        return contactPerson;
	}



}
