package localEntities;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@RequestScoped
@Named(value = "searchParam")
public class SearchParam {
	
	private String typeOfIndustry;
	private int noOfProductsBrought;
		
	public SearchParam() {
	
	}
	public SearchParam(String typeOfIndustry, int noOfProductsBrought) {
		super();
		this.typeOfIndustry = typeOfIndustry;
		this.noOfProductsBrought = noOfProductsBrought;
	}
	public String getTypeOfIndustry() {
		return typeOfIndustry;
	}
	public void setTypeOfIndustry(String typeOfIndustry) {
		this.typeOfIndustry = typeOfIndustry;
	}
	public int getNoOfProductsBrought() {
		return noOfProductsBrought;
	}
	public void setNoOfProductsBrought(int noOfProductsBrought) {
		this.noOfProductsBrought = noOfProductsBrought;
	}
	
	
	

}
