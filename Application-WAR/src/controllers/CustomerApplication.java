package controllers;

import java.util.ArrayList;
import javax.enterprise.context.ApplicationScoped;

import javax.inject.Named;

import localEntities.SearchParam;
import repository.entities.ContactPerson;
import repository.entities.Customer;
import repository.entities.IndustryType;
import repository.entities.User;
import mbeans.CustomerManagedBean;

import javax.el.ELContext;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;

/**
 * The class is a demonstration of how the application scope works. You can
 * change this scope.
 *
 * @author Flloyd Dsouza
 */
@Named(value = "customerApplication")
@ApplicationScoped
public class CustomerApplication {

    //dependency injection of managed bean here so that we can use its methods
    @ManagedProperty(value = "#{customerManagedBean}")
    CustomerManagedBean customerManagedBean;

    private ArrayList<Customer> customers;
    private ArrayList<ContactPerson> contactPersons;
    private ArrayList<IndustryType> industryTypes;
    private ArrayList<User> users;


    private boolean showForm = true;

    public boolean isShowForm() {
        return showForm;
    }

    // Add some property data from db to app 
    public CustomerApplication() throws Exception {
        customers = new ArrayList<>();
        contactPersons = new ArrayList<>();
        industryTypes = new ArrayList<>();
        users =  new ArrayList<>();
       
        //instantiate customerManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "customerManagedBean");

        //get properties from db 
        updateCustomerList();
        updateContactPersonList();
        updateIndustryTypeList();
        updateUserList();
        
    }

    public ArrayList<Customer> getCustomers() {
        return customers;
    }

    private void setCustomers(ArrayList<Customer> newCustomers) {
        this.customers = newCustomers;
    }
    
    

    public ArrayList<ContactPerson> getContactPersons() {
		return contactPersons;
	}

	public void setContactPersons(ArrayList<ContactPerson> contactPersons) {
		this.contactPersons = contactPersons;
	}
	
	
	
	 public ArrayList<IndustryType> getIndustryTypes() {
		return industryTypes;
	}

	public void setIndustryTypes(ArrayList<IndustryType> industryTypes) {
		this.industryTypes = industryTypes;
	}
	

	public ArrayList<User> getUsers() {
		return users;
	}

	public void setUsers(ArrayList<User> users) {
		this.users = users;
	}

	public ArrayList<Customer> getCustomersOfUser(String user) {
		 
		 updateCustomerList();
		 
		 if(user.equals("admin")) {			
			 return customers;
		 }else {
		 
		
		 ArrayList<Customer> customerOfUserList =  new ArrayList<>();
		 for (int i = 0; i < customers.size(); i++) {
			 try {
				 if (customers.get(i).getUserId().equals(user)) {
					 customerOfUserList.add(customers.get(i));
				 }
			 }catch(Exception e) {
				 
			 }
		 }		 
		 customers = customerOfUserList;		 
	     return customers;
		 }
	  }

	
	public void combinationSearch(SearchParam searchParam,String user){		
		
		String type = searchParam.getTypeOfIndustry();
		int noOfProducts = searchParam.getNoOfProductsBrought();
		
		boolean isAdmin = false;
		if(user.equals("admin")) {
			isAdmin = true;
		}
							
		customers.clear();		
		ArrayList<Customer> searchResults =  new ArrayList<Customer>();		
		for (repository.entities.Customer customer : customerManagedBean.combinationSearch(type, noOfProducts)) {					
			if (isAdmin) {				
				searchResults.add(customer);
			}else {
				if (customer.getUserId().equals(user)) {
					searchResults.add(customer);
				 }
			}
	
		}	
		setCustomers(searchResults);		
				
	}
	
	
	

    public void updateCustomerList() {
        
            customers.clear();

            for (repository.entities.Customer customer : customerManagedBean.getAllCustomers())
            {
                customers.add(customer);
            }

            setCustomers(customers);
        
    }
    
    
    public void updateContactPersonList() {
    
    		contactPersons.clear();
    		for (repository.entities.ContactPerson contactPerson : customerManagedBean.getAllContactPeople()) {
    			contactPersons.add(contactPerson);
    		}
    		
    		setContactPersons(contactPersons);
    	
    }
    
    
    public void updateIndustryTypeList() {
        
		
    	industryTypes.clear();
		for (repository.entities.IndustryType industryType : customerManagedBean.getAllIndustryType()) {
			 industryTypes.add(industryType);			
		}
		
		setIndustryTypes(industryTypes);

    }

    
    public void updateUserList() {
        
		
    	users.clear();
		for (repository.entities.User user : customerManagedBean.getAllUsers()) {
			if (!user.getUsername().equals("admin") )
			{
			users.add(user);
			}
		}
		
		setUsers(users);

    }
    

    public void searchCustomerById(int customerId) {
        customers.clear();

        customers.add(customerManagedBean.searchCustomerById(customerId));
    }

    
    
    public void searchAll()
    {
        customers.clear();
        
        for (repository.entities.Customer customer : customerManagedBean.getAllCustomers())
        {
            customers.add(customer);
        }
        
        setCustomers(customers);
    }
    
   
}
