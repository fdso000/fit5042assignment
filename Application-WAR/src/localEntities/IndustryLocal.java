package localEntities;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@RequestScoped
@Named(value = "industry")
public class IndustryLocal {
	
	private String industryType;
	
	public IndustryLocal() {
		
	}
	
	public IndustryLocal(String industryType) {
		super();
		this.industryType = industryType;
	}
	
	
	public String getIndustryType() {
		return industryType;
	}

	public void setIndustryType(String industryType) {
		this.industryType = industryType;
	}


	

}
