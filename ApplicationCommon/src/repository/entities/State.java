package repository.entities;

public enum State {
	
	QLD ("Queensland"),
	SA ("South Australia"),
	TAS ("Tasmania"),
	VIC ("Victoria"),
	NSW ("New South Wales"),
	WA ("Western Australia");
	
	 private String label;

	    private State(String label) {
	        this.label = label;
	    }

	    public String getLabel() {
	        return label;
	    }
}
