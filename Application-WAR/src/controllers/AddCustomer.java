package controllers;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import mbeans.CustomerManagedBean;

import javax.faces.bean.ManagedProperty;

/**
 *
 * @author Flloyd Dsouza
 */
@RequestScoped
@Named("addCustomer")
public class AddCustomer {

    @ManagedProperty(value = "#{customerManagedBean}")
    CustomerManagedBean customerManagedBean;

    private boolean showForm = true;

    private Customer customer;

    CustomerApplication app;

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Customer getCustomer() {
        return customer;
    }

    public boolean isShowForm() {
        return showForm;
    }

    public AddCustomer() {
        ELContext context
                = FacesContext.getCurrentInstance().getELContext();

        app = (CustomerApplication) FacesContext.getCurrentInstance()
                .getApplication()
                .getELResolver()
                .getValue(context, null, "customerApplication");

     
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "customerManagedBean");
    }

    public String addCustomer(Customer localCustomer) {
    	Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.INFO, "Enter Func");
        try {
           
        	Date date = new Date(System.currentTimeMillis());
        	localCustomer.setDateAdded(date);
        	
        	if(localCustomer.getUserId() == null) {
        		String userId =  FacesContext.getCurrentInstance().getExternalContext().getRemoteUser();        		
        		localCustomer.setUserId(userId);
        	}
                	
        	Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.INFO, "Adding By" + localCustomer.getUserId());
        	
        	customerManagedBean.addCustomer(localCustomer);

            //Refresh the list
             app.searchAll();
             app.updateContactPersonList();                      
             Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.INFO, "Added Successfully");
             return "index";
        } catch (Exception ex) {
        	Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.INFO, "Adding Failed" + ex);
        }
        showForm = true;
		return null;
    }

}
