package controllers;

import javax.el.ELContext;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Flloyd Dsouza
 */
@Named(value = "customerController")
@RequestScoped
public class CustomerController {

    private int customerId; //this customerId is the index, don't confuse with the customer Id


    public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	
	private repository.entities.Customer customer;

    public CustomerController() {
        // Assign customer identifier via GET param 
        //this customerID is the index, don't confuse with the customer Id
    	customerId = Integer.valueOf(FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("customerID"));
        // Assign customer based on the id provided 
        customer = getCustomer();
    }

    public repository.entities.Customer getCustomer() {
    	  if (customer == null) {
            // Get application context bean customerApplication 
            ELContext context
                    = FacesContext.getCurrentInstance().getELContext();
            CustomerApplication app
                    = (CustomerApplication) FacesContext.getCurrentInstance()
                            .getApplication()
                            .getELResolver()
                            .getValue(context, null, "customerApplication");
            // -1 to customerId since we +1 in JSF (to always have positive customer Id!) 
            return app.getCustomers().get(--customerId); //this customerId is the index, don't confuse with the customer Id
    	  }
          return customer;
    }
    
	public String destroy() throws Throwable {
		finalize();
		return "index";
	}
	
	 public String logout()
	    {
	               FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
	            return "login";
	    }
	
}
