package repository.entities;

public enum Scale {
	
	SMALL ("Small"),
	MEDIUM ("Medium"),
	LARGE ("Large"),
;
	
	 private String label;

	    private Scale(String label) {
	        this.label = label;
	    }

	    public String getLabel() {
	        return label;
	    }

}
