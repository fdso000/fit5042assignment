package mbeans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import repository.CustomerRepository;
import repository.entities.*;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
/**
*
* @author Flloyd Dsouza
*/

@ManagedBean(name = "customerManagedBean")
@SessionScoped
public class CustomerManagedBean implements Serializable{

	
	@EJB
	CustomerRepository customerRepository;
	
	
	public CustomerManagedBean() {
		
	}
	
	public List<Customer> getAllCustomers() {
        try {
            List<Customer> customers = customerRepository.getAllCustomers();
            return customers;
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

	
	public void addCustomer(Customer customer) {
	        try {
	        	customerRepository.addCustomer(customer);
	        } catch (Exception ex) {
	            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
	        }
	}
	

	public List<Customer> combinationSearch(String industryType, int noOfProducts){
		 try {
	            List<Customer> customers = customerRepository.searchCustomerByTypeAndProduct(industryType, noOfProducts);
	            return customers;
	        } catch (Exception ex) {
	            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
	        }
	        return null;
		
	}
	

    public Customer searchCustomerById(int id) {
        try {
            return customerRepository.searchCustomerById(id);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }
	
    public List<ContactPerson> getAllContactPeople(){    	
        try {
        	List<ContactPerson> contactPersons = customerRepository.getAllContactPeople();
        	return contactPersons;
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }
    
   
    
    
    public void removeCustomer(int customerId) {
        try {
        	customerRepository.removeCustomer(customerId);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
	
    public void addCustomer(controllers.Customer localCustomer) {
       Logger.getLogger("localcustomer + "+localCustomer.getContactPersonsString());
    	Customer customer = convertCustomerToEntity(localCustomer); 

        try {
        	customerRepository.addCustomer(customer);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String editCustomer(Customer customer) {
        try {
            customerRepository.editCustomer(customer);
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.INFO, "Updated Successfully");
            return "index";
            
        } catch (Exception ex) {
        	Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.INFO, "Update Failed!");
        }
		return null;
    }
    
    
    //Industry Type
    
    public List<IndustryType> getAllIndustryType(){    	
        try {
        	List<IndustryType> industryTypes = customerRepository.getAllIndustryType();
        	return industryTypes;
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }
       
    public void addIndustryType(IndustryType industryType) {	
		try{
			customerRepository.addIndustryType(industryType);			
		}catch (Exception ex) {
			Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }		
	}
    
    public void removeIndustryType(int industryTypeId) {
        try {
        	customerRepository.removeIndustryType(industryTypeId);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
  
    //Users 
    
    public List<User> getAllUsers() {
        try {
            List<User> users = customerRepository.getAllUsers();
            return users;
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    
    public void addUser(User user) {	
		try{
			customerRepository.addUser(user);					
		}catch (Exception ex) {
			Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }		
	}
    
    public void removeUser(int id) {
        try {
        	customerRepository.removeUser(id);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String editUser(User user, String newPassword) {
    	if(!newPassword.isEmpty()) {
    		Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.INFO, "New Password" + newPassword);
    	 
	    	try {
				user.setPassword(hashPassword(newPassword));
			} catch (NoSuchAlgorithmException e) {			
				e.printStackTrace();
			}
	
    	}
        try {
            customerRepository.editUser(user);       
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.INFO, "User Updated Successfully");
            return "users";
            
        } catch (Exception ex) {
        	Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.INFO, "User Update Failed!");
        }
		return null;
    }
    
    
    private String hashPassword(String password) throws NoSuchAlgorithmException {
		MessageDigest digest = MessageDigest.getInstance("SHA-256");
		byte[] encodedhash = digest.digest(
				password.getBytes(StandardCharsets.UTF_8));
		
		String hashValue = bytesToHex(encodedhash);
		return hashValue;
	}
	
	private static String bytesToHex(byte[] hash) {
	    StringBuilder hexString = new StringBuilder(2 * hash.length);
	    for (int i = 0; i < hash.length; i++) {
	        String hex = Integer.toHexString(0xff & hash[i]);
	        if(hex.length() == 1) {
	            hexString.append('0');
	        }
	        hexString.append(hex);
	    }
	    return hexString.toString();
	}
	
    
    
    private Customer convertCustomerToEntity(controllers.Customer localCustomer) {
    	
    	Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.INFO, "String from dummy input"+ localCustomer.getContactPersonsString());
    	
    	Customer customer = new Customer(); //entity
        String streetNumber = localCustomer.getStreetNumber();
        String streetAddress = localCustomer.getStreetAddress();
        String suburb = localCustomer.getSuburb();
        String postcode = localCustomer.getPostcode();
        String state = localCustomer.getState();
        
        Address address = new Address(streetNumber, streetAddress, suburb, postcode, state);
        customer.setCompanyAddress(address);
        
        
        String[] contactStrings;
        
        try{
        	contactStrings = localCustomer.getContactPersonsString().split(";");
        }catch(Exception e) {
        	contactStrings = null;
        }
        
        if (contactStrings != null) {
        	
        String[] contactAttributes;
        	for(int i = 0; i < contactStrings.length; i++) {
        		
        		try {
        			contactAttributes = contactStrings[i].split(",");
        			ContactPerson contactperson = new ContactPerson();
        			
        			contactperson.setName(contactAttributes[0]);
        			contactperson.setGender(contactAttributes[1]);
        			
        			
        			Address personAddress =  new Address();
        			personAddress.setStreetNumber(contactAttributes[3]);
        			personAddress.setStreetAddress(contactAttributes[4]);
        			personAddress.setSuburb(contactAttributes[5]);
        			personAddress.setPostcode(contactAttributes[6]);
        			personAddress.setState(contactAttributes[7]);
        			
        			try {
        				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd", Locale.ENGLISH);      			
        				Date dateOfBirth = formatter.parse(contactAttributes[2]);
        				contactperson.setDateOfBirth(dateOfBirth);
        			}catch(Exception e) {
        				Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.INFO, "Date Parsing Error"); 
        			}
        			
        			contactperson.setAddress(personAddress);
        			contactperson.setPhoneNumber(contactAttributes[8]);
        			contactperson.setEmail(contactAttributes[9]);
        			
        			Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.INFO, "contact +"+ contactperson); 
        			customer.addContactPerson(contactperson);
        			
        		} catch(Exception e) {
        			Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.INFO, "Error While Parsing Contact Person");
        		}
        		
        		
        	}
        	
        	
        	
        }
        
                
        customer.setCustomerName(localCustomer.getCustomerName());
        customer.setCompanyPhone(localCustomer.getCompanyPhone());
        customer.setWebsite(localCustomer.getWebsite());
        customer.setTypeOfIndustry(localCustomer.getTypeOfIndustry());
        customer.setIndustryScale(localCustomer.getIndustryScale());
        customer.setNoOfProductsBrought(localCustomer.getNoOfProductsBrought());
        customer.setDateAdded(localCustomer.getDateAdded());
        customer.setUserId(localCustomer.getUserId());
      
        
        return customer;
    }
    
    public String logout()
    {
    		Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.INFO, "Logging Out User" + FacesContext.getCurrentInstance().getExternalContext() );
            FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
            return "login";
    }
    
    public void logoutAdmin()
    {
    		Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.INFO, "Logging Out User" + FacesContext.getCurrentInstance().getExternalContext() );
            FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
            
    }
    
}
