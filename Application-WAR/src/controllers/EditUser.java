package controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import repository.entities.User;

/**
*
* @author Flloyd Dsouza
*/
@Named(value = "editUser")
@RequestScoped
public class EditUser {

	private int userId;
	
	private User user;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	public EditUser() {
		
		userId = Integer.valueOf(FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("userId"));
        // Assign customer based on the id provided 
		user = getUser();
		
	}
	
	
	public User getUser() {
  	  if (user == null) {         
          ELContext context = FacesContext.getCurrentInstance().getELContext();
          CustomerApplication app
                  = (CustomerApplication) FacesContext.getCurrentInstance()
                          .getApplication()
                          .getELResolver()
                          .getValue(context, null, "customerApplication");          
          return app.getUsers().get(--userId);       		 
  	  }
        return user;
  }
	
}
