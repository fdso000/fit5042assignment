package repository;

import java.util.List;

import repository.entities.ContactPerson;
import repository.entities.Customer;
import repository.entities.Groups;
import repository.entities.IndustryType;
import repository.entities.User;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;



/*
 * @author Flloyd Dsouza
 * 
 */

@Stateless
public class JPACustomerRepositoryImpl implements CustomerRepository{

	@PersistenceContext(unitName = "Application-ejbPU")
    private EntityManager entityManager;
	
	@Override
	public void addCustomer(Customer customer) throws Exception {
		 
		
		try {
			List<Customer> customers = entityManager.createNamedQuery(Customer.GET_ALL_QUERY_NAME).getResultList();
			customer.setCustomerId(customers.get(0).getCustomerId() + 1);
		} catch (Exception e) {
			customer.setCustomerId(1);
		}
		
		int ContactId;
		
		try {
			 List<ContactPerson> contactPersons =  entityManager.createNamedQuery(ContactPerson.GET_ALL_QUERY_NAME).getResultList();		 
			 ContactId = contactPersons.get(0).getContactPersonId() +1;
		} catch(Exception e) {
			ContactId =1;
		}
		 
		
		 
		 for(int i = 0; i < customer.getContactPersons().size(); i++) {
			 customer.getContactPersons().get(i).setContactPersonId(ContactId);
			 customer.getContactPersons().get(i).setCustomer(customer);
			 ContactId++;
		 }
		 
	     entityManager.persist(customer);
	    
		
	}
	
	

	@Override
	public Customer searchCustomerById(int id) throws Exception {
		Customer customer = entityManager.find(Customer.class, id);
	     return customer;
	}
	
	@Override
	public List<Customer> searchCustomerByTypeAndProduct(String type, int noOfProducts) throws Exception {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery query = builder.createQuery(Customer.class);
		Root<Customer> p = query.from(Customer.class);
		query.select(p).where(builder.greaterThanOrEqualTo(p.get("noOfProductsBrought").as(Integer.class), noOfProducts),
						      builder.equal(p.get("typeOfIndustry").as(String.class), type));
				
		List<Customer> customerList = entityManager.createQuery(query).getResultList();
		return customerList;
	}

	@Override
	public List<Customer> getAllCustomers() throws Exception {
		 return entityManager.createNamedQuery(Customer.GET_ALL_QUERY_NAME).getResultList();
	}

	@Override
	public void removeCustomer(int customerId) throws Exception {
		Customer c = searchCustomerById(customerId);
    	if(c!=null) {
    		entityManager.remove(c);
    	}
		
	}

	@Override
	public void editCustomer(Customer customer) throws Exception {
		 try {
	            entityManager.merge(customer);
	        } catch (Exception ex) {

	        }
		
	}

	@Override
	public List<ContactPerson> getAllContactPeople() throws Exception {
		 return entityManager.createNamedQuery(ContactPerson.GET_ALL_QUERY_NAME).getResultList();
	}

	
	
	
	
	//Industry Type CRUD
	
	
	@Override
	public void addIndustryType(IndustryType industryType) throws Exception {
		
		try {
			List<IndustryType> industryTypes = entityManager.createNamedQuery(IndustryType.GET_ALL_QUERY_NAME).getResultList();
			industryType.setIndustryId(industryTypes.get(0).getIndustryId() + 1 );
		} catch (Exception e) {
			industryType.setIndustryId(1);
		}
			
        entityManager.persist(industryType);
	
	}
	
	
	@Override
	public List<IndustryType> getAllIndustryType() throws Exception {
		return entityManager.createNamedQuery(IndustryType.GET_ALL_QUERY_NAME).getResultList();
	}

	@Override
	public void removeIndustryType(int industryTypeId) throws Exception {
		IndustryType i = searchIndustryTypeId(industryTypeId);
    	if(i != null) {
    		entityManager.remove(i);
    	}
		
	}

	@Override
	public IndustryType searchIndustryTypeId(int id) throws Exception {
		IndustryType industryType = entityManager.find(IndustryType.class, id);		
	    return industryType;
	}







	//User CRUD
	
	@Override
	public List<User> getAllUsers() throws Exception {
		return entityManager.createNamedQuery(User.GET_ALL_QUERY_NAME).getResultList();
	}
	
		
		
	


	@Override
	public void addUser(User user) throws Exception {
		
		try {
			List<User> users = entityManager.createNamedQuery(User.GET_ALL_QUERY_NAME).getResultList();			
			user.setId(users.get(0).getId() + 1);
			
		} catch (Exception e) {
			user.setId(1);
		}
		
        entityManager.persist(user);
        
        Groups group =  new Groups();
        group.setUsername(user.getUsername());
        group.setGroupname("user");
        
        entityManager.persist(group);
		
	}

	@Override
	public void removeUser(int id) throws Exception {
		User u = searchUserById(id);
    	if(u != null) {
    		entityManager.remove(u);
    		
    		Groups group = findGroupOfUser(u.getUsername());
    		if(group != null) {
    			entityManager.remove(group);
    		}
    	}	
	}

	
	public Groups findGroupOfUser(String username) {
		
		List<Groups> groups = entityManager.createNamedQuery(Groups.GET_ALL_QUERY_NAME).getResultList();
		if(groups.size() > 0) {
			
			for( int i = 0; i < groups.size(); i++) {
				if (groups.get(i).getUsername().equals(username)) {
					return groups.get(i);
				}
			}
			
		}
		return null;
	}


	@Override
	public User searchUserById(int id) throws Exception {		
		User user = entityManager.find(User.class, id);		
	    return user;
	}



	@Override
	public void editUser(User user) throws Exception {
		 try {
	            entityManager.merge(user);
	        } catch (Exception ex) {

	        }	
	}



	
	
}
