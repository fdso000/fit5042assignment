package mbeans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import repository.entities.Gender;
import repository.entities.Scale;
import repository.entities.State;

@ManagedBean(name="data")
@SessionScoped
public class Data {


    public Scale[] getScale() {
        return Scale.values();
    }

    public State[] getState() {
        return State.values();
    }
    
    public Gender[] getGender() {
        return Gender.values();
    }
}