package controllers;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Named;


import localEntities.UserLocal;
import mbeans.CustomerManagedBean;
import repository.entities.User;

@RequestScoped
@Named("userController")
public class UserController {


    @ManagedProperty(value = "#{customerManagedBean}")
    CustomerManagedBean customerManagedBean;

    CustomerApplication app;

	public UserController() {
		
		ELContext context = FacesContext.getCurrentInstance().getELContext();

		app = (CustomerApplication) FacesContext.getCurrentInstance()
												.getApplication()
												.getELResolver()
												.getValue(context, null, "customerApplication");
		
		 ELContext elContext = FacesContext.getCurrentInstance().getELContext();
	     customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication()
	                											 .getELResolver()
	                											 .getValue(elContext, null, "customerManagedBean");
	        		
	}
	
	
	public String addUser(UserLocal localUser) {

		User user = new User();
		user.setName(localUser.getName());
		user.setEmail(localUser.getEmail());
		user.setPhoneNumber(localUser.getPhoneNumber());
		user.setUsername(localUser.getUsername());
		
		try {
			user.setPassword(hashPassword(localUser.getPassword()));
		} catch (NoSuchAlgorithmException e1) {
			//hash value for "1234"
			user.setPassword("03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4");			
		}
			
		try {			
			customerManagedBean.addUser(user);
			app.updateUserList();
			Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.INFO, "Adding User Success: " + user );
			return "users";
			
		}catch(Exception e) {
			Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, "Adding User Failed");
		}
		
		return null;
	
	}
	
	
	public void removeUser (int id) {
        try {           
        	customerManagedBean.removeUser(id);      
            app.updateUserList();
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.INFO, "Deleting User Success: " + id );
        } catch (Exception e) {
        	Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, "Deleting User Failed");
        }
    }

	private String hashPassword(String password) throws NoSuchAlgorithmException {
		MessageDigest digest = MessageDigest.getInstance("SHA-256");
		byte[] encodedhash = digest.digest(
				password.getBytes(StandardCharsets.UTF_8));
		
		String hashValue = bytesToHex(encodedhash);
		return hashValue;
	}
	
	private static String bytesToHex(byte[] hash) {
	    StringBuilder hexString = new StringBuilder(2 * hash.length);
	    for (int i = 0; i < hash.length; i++) {
	        String hex = Integer.toHexString(0xff & hash[i]);
	        if(hex.length() == 1) {
	            hexString.append('0');
	        }
	        hexString.append(hex);
	    }
	    return hexString.toString();
	}
	
}
