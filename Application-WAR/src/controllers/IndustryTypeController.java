package controllers;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import localEntities.IndustryLocal;
import mbeans.CustomerManagedBean;
import repository.entities.IndustryType;

/**
*
* @author Flloyd Dsouza
*/
@RequestScoped
@Named("industryTypeController")
public class IndustryTypeController {
	
    @ManagedProperty(value = "#{customerManagedBean}")
    CustomerManagedBean customerManagedBean;

    CustomerApplication app;

	public IndustryTypeController() {
		
		ELContext context = FacesContext.getCurrentInstance().getELContext();

		app = (CustomerApplication) FacesContext.getCurrentInstance()
												.getApplication()
												.getELResolver()
												.getValue(context, null, "customerApplication");
		
		 ELContext elContext = FacesContext.getCurrentInstance().getELContext();
	     customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication()
	                											 .getELResolver()
	                											 .getValue(elContext, null, "customerManagedBean");
	        		
	}
	
	
	public void addIndustryType(IndustryLocal localIndustryType) {


		IndustryType industryType = new IndustryType();
		industryType.setIndustryType(localIndustryType.getIndustryType());
		
		
		try {			
			customerManagedBean.addIndustryType(industryType);
			app.updateIndustryTypeList();
			Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.INFO, "Adding Industry Success: " + industryType );
			
		}catch(Exception e) {
			Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, "Adding Industry Type Failed");
		}
	
	}
	
	
	public void removeIndustryType (int industryTypeId) {
        try {           
        	customerManagedBean.removeIndustryType(industryTypeId);         
            app.updateIndustryTypeList();
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.INFO, "Deleting Industry Type Success: " + industryTypeId );
        } catch (Exception ex) {
        	Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, "Deleting Industry Type Failed");
        }
    }


}
