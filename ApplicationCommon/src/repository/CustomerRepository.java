package repository;

import java.util.List;

import repository.entities.ContactPerson;
import repository.entities.Customer;
import repository.entities.Groups;
import repository.entities.IndustryType;
import repository.entities.User;

/*
 * @author Flloyd Dsouza
 * Interface for Customer Repository EJB Module
 * 
 */
public interface CustomerRepository {
	
	
	//Customer CRUD
	
    public void addCustomer(Customer customer) throws Exception;
   
    public Customer searchCustomerById(int id) throws Exception;
  
    public List<Customer> getAllCustomers() throws Exception;
     
    public void removeCustomer(int customerId) throws Exception;
   
    public void editCustomer(Customer customer) throws Exception;
    
    public List<Customer> searchCustomerByTypeAndProduct(String type, int noOfProducts) throws Exception;
 
    
    //Contact Person List
    public List<ContactPerson> getAllContactPeople() throws Exception;
    
   
    // Industry Type CRUD
    
    public List<IndustryType> getAllIndustryType() throws Exception;
    
    public void addIndustryType(IndustryType industryType) throws Exception;
    
    public void removeIndustryType(int industryTypeId) throws Exception;
    
    public IndustryType searchIndustryTypeId(int id) throws Exception;
    
    //User CRUD
    
    public List<User> getAllUsers() throws Exception;
    
    public void addUser(User user) throws Exception;
    
    public User searchUserById(int id) throws Exception;
    
    public void removeUser(int id) throws Exception;
    
    public void editUser(User user) throws Exception;

}
