package repository.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/*
 * @author Flloyd Dsouza
 * 
 */


@Entity
@NamedQueries({
@NamedQuery(name = IndustryType.GET_ALL_QUERY_NAME, query = "SELECT c FROM IndustryType c order by c.industryId desc")})

public class IndustryType implements Serializable {
	  
	public static final String GET_ALL_QUERY_NAME = "IndustryType.getAll";
	
	private int industryId;
	private String industryType;
	
	
	public IndustryType() {
		
	}
	
	public IndustryType(int industryId, String industryType) {
		this.industryId = industryId;
		this.industryType = industryType;
	}
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "industry_id")	
	public int getIndustryId() {
		return industryId;
	}
	
	public void setIndustryId(int industryId) {
		this.industryId = industryId;
	}
	
	@Column(name = "industry_type_name")	
	public String getIndustryType() {
		return industryType;
	}
	
	public void setIndustryType(String industryType) {
		this.industryType = industryType;
	}

	@Override
	public String toString() {
		return "IndustryType [industryId=" + industryId + ", industryType=" + industryType + "]";
	}
	

}
