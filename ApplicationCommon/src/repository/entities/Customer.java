package repository.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

/*
 * @author Flloyd Dsouza
 * 
 */
@Entity
@NamedQueries({
    @NamedQuery(name = Customer.GET_ALL_QUERY_NAME, query = "SELECT c FROM Customer c order by c.customerId desc")})

public class Customer implements Serializable {

	public static final String GET_ALL_QUERY_NAME = "Customer.getAll";
	
    private int customerId;
    private String customerName;
    private Address companyAddress;
    private int companyPhone;
    private String website;
    private String typeOfIndustry;
    private String industryScale;
    private int noOfProductsBrought;
    private Date dateAdded;
    private String userId;
    
    
    private List<ContactPerson> contactPersons;


    public Customer() {
    	this.contactPersons = new ArrayList<>();   	
    }
    
    public Customer(int customerId, String customerName, Address companyAddress, int companyPhone, String website, String typeOfIndustry, String industryScale, int noOfProductsBrought, Date dateAdded, String userId) {
        this.customerId = customerId;
        this.customerName = customerName;
        this.companyAddress = companyAddress;
        this.companyPhone = companyPhone;
        this.website = website;
        this.typeOfIndustry = typeOfIndustry;
        this.industryScale = industryScale;
        this.noOfProductsBrought = noOfProductsBrought;
        this.dateAdded = dateAdded;
        this.userId = userId;
        this.contactPersons = new ArrayList<>();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "customer_id")
    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    @Column(name = "name")
    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    @Embedded
    public Address getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(Address companyAddress) {
        this.companyAddress = companyAddress;
    }

    @Column(name = "phone_number")
    public int getCompanyPhone() {
        return companyPhone;
    }

    public void setCompanyPhone(int companyPhone) {
        this.companyPhone = companyPhone;
    }

    @Column(name = "website")
    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    @Column(name = "type_of_industry")
    public String getTypeOfIndustry() {
        return typeOfIndustry;
    }

    public void setTypeOfIndustry(String typeOfIndustry) {
        this.typeOfIndustry = typeOfIndustry;
    }

    @Column(name = "industry_scale")
    public String getIndustryScale() {
        return industryScale;
    }

    public void setIndustryScale(String industryScale) {
        this.industryScale = industryScale;
    }

    @Column(name = "no_of_Products")
    public int getNoOfProductsBrought() {
        return noOfProductsBrought;
    }

    public void setNoOfProductsBrought(int noOfProductsBrought) {
        this.noOfProductsBrought = noOfProductsBrought;
    }

    @Column(name = "date_added")
    public Date getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }

    
    @OneToMany(mappedBy = "customer",cascade = CascadeType.ALL, orphanRemoval = true)
    public List<ContactPerson> getContactPersons() {
  		return contactPersons;
  	}

  	public void setContactPersons(List<ContactPerson> contactPersons) {
  		this.contactPersons = contactPersons;
  	}

	
	public void addContactPerson(ContactPerson person) {
		this.contactPersons.add(person);
	}
       

	public String dateAddedToString() {
  		if (dateAdded != null) {
  			DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");  
            String strDate = dateFormat.format(dateAdded);
            return strDate;
  		}
  		return "";
  	}
	
	@Column(name = "userid")
    public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Override
    public String toString() {
        return "Customer{" +
                "customerId=" + customerId +
                ", customerName='" + customerName + '\'' +
                ", companyAddress=" + companyAddress +
                ", companyPhone=" + companyPhone +
                ", website='" + website + '\'' +
                ", typeOfIndustry='" + typeOfIndustry + '\'' +
                ", industryScale='" + industryScale + '\'' +
                ", noOfProductsBrought=" + noOfProductsBrought +
                ", dateAdded=" + dateAdded ;
    }
}

