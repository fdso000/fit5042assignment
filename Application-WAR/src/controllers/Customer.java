/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import repository.entities.Address;
import repository.entities.ContactPerson;


import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 *
 * @author Flloyd Dsouza
 * The Local Customer Class
 */
@RequestScoped
@Named(value = "customer")
public class Customer implements Serializable {

    private int customerId;
    private String customerName;
    private Address companyAddress;
    private int companyPhone;
    private String website;
    private String typeOfIndustry;
    private String industryScale;
    private int noOfProductsBrought;
    private Date dateAdded;
    private String userId;
    
    
    private List<ContactPerson> contactPersons;
    private String contactPersonsString;
   

    private String streetNumber;
    private String streetAddress;
    private String suburb;
    private String postcode;
    private String state;
    
   
    public Customer() {   	
    	this.contactPersons = new LinkedList();
    }

	public Customer(int customerId, String customerName, Address companyAddress, int companyPhone, String website,
			String typeOfIndustry, String industryScale, int noOfProductsBrought, Date dateAdded,
			List<ContactPerson> contactPersons, String userId ) {
		
		this.customerId = customerId;
		this.customerName = customerName;
		this.companyAddress = companyAddress;
		this.companyPhone = companyPhone;
		this.website = website;
		this.typeOfIndustry = typeOfIndustry;
		this.industryScale = industryScale;
		this.noOfProductsBrought = noOfProductsBrought;
		this.dateAdded = dateAdded;
		this.userId = userId;
		this.contactPersons = (List<ContactPerson>)contactPersons;		
	}

	//=================================================================================
	
	
	
    public String getStreetNumber() {
        return streetNumber;
    }


	public String getContactPersonsString() {
		return contactPersonsString;
	}

	public void setContactPersonsString(String contactPersonsString) {
		this.contactPersonsString = contactPersonsString;
	}

	public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getSuburb() {
        return suburb;
    }

    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }


	//=================================================================================
	
	
	public int getCustomerId() {
		return customerId;
	}

	

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Address getCompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(Address companyAddress) {
		this.companyAddress = companyAddress;
	}

	public int getCompanyPhone() {
		return companyPhone;
	}

	public void setCompanyPhone(int companyPhone) {
		this.companyPhone = companyPhone;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getTypeOfIndustry() {
		return typeOfIndustry;
	}

	public void setTypeOfIndustry(String typeOfIndustry) {
		this.typeOfIndustry = typeOfIndustry;
	}

	public String getIndustryScale() {
		return industryScale;
	}

	public void setIndustryScale(String industryScale) {
		this.industryScale = industryScale;
	}

	public int getNoOfProductsBrought() {
		return noOfProductsBrought;
	}

	public void setNoOfProductsBrought(int noOfProductsBrought) {
		this.noOfProductsBrought = noOfProductsBrought;
	}

	public Date getDateAdded() {
		return dateAdded;
	}

	public void setDateAdded(Date dateAdded) {
		this.dateAdded = dateAdded;
	}

	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	
	
	public List<ContactPerson> getContactPersons() {
		return contactPersons;
	}

	public void setContactPersons(List<ContactPerson> contactPersons) {
		this.contactPersons = contactPersons;
	}

	@Override
	public String toString() {
		return "Customer [customerId=" + customerId + ", customerName=" + customerName + ", companyAddress="
				+ companyAddress + ", companyPhone=" + companyPhone + ", website=" + website + ", typeOfIndustry="
				+ typeOfIndustry + ", industryScale=" + industryScale + ", noOfProductsBrought=" + noOfProductsBrought
				+ ", dateAdded=" + dateAdded + ", userId=" + userId + ", contactPersons=" + contactPersons
				+ ", contactPersonsString=" + contactPersonsString + ", streetNumber=" + streetNumber
				+ ", streetAddress=" + streetAddress + ", suburb=" + suburb + ", postcode=" + postcode + ", state="
				+ state + "]";
	}




   
	
    
}
