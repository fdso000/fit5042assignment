package repository.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "GROUPS")
@NamedQueries({
    @NamedQuery(name = Groups.GET_ALL_QUERY_NAME, query = "SELECT g FROM Groups g order by g.id desc")})
public class Groups implements Serializable{

public static final String GET_ALL_QUERY_NAME = "Groups.getAll";
	
    private int id;	
    private String username;
    private String groupname;
	
	
	
	public Groups() {	
	}


	public Groups(int id, String username, String groupname) {	
		this.id = id;
		this.username = username;
		this.groupname = groupname;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "id")
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "username")
	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	@Column(name = "groupname")
	public String getGroupname() {
		return groupname;
	}


	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}


	@Override
	public String toString() {
		return "Groups [id=" + id + ", username=" + username + ", groupname=" + groupname + "]";
	}
		

	
}
