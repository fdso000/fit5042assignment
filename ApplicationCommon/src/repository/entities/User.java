package repository.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/*
 * @author Flloyd Dsouza
 * 
 */

@Entity
@Table(name = "USERS")
@NamedQueries({
    @NamedQuery(name = User.GET_ALL_QUERY_NAME, query = "SELECT u FROM User u order by u.id desc")})
public class User implements Serializable {
	
	public static final String GET_ALL_QUERY_NAME = "User.getAll";
	
    private int id;	
    private String username;
    private String password;
    private String name;
    private String email;
    private int phoneNumber;
	
	
	
	public User() {
	}


	public User(int id, String username, String password, String name, String email, int phoneNumber) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.name = name;
		this.email = email;
		this.phoneNumber = phoneNumber;
	}


	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "id")
	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}


	@Column(name = "username")
	public String getUsername() {
		return username;
	}



	public void setUsername(String username) {
		this.username = username;
	}


	@Column(name = "password")
	public String getPassword() {
		return password;
	}



	public void setPassword(String password) {
		this.password = password;
	}



	public String getName() {
		return name;
	}





	public void setName(String name) {
		this.name = name;
	}





	public String getEmail() {
		return email;
	}





	public void setEmail(String email) {
		this.email = email;
	}





	public int getPhoneNumber() {
		return phoneNumber;
	}





	public void setPhoneNumber(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}





	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", password=" + password + ", name=" + name + ", email="
				+ email + ", phoneNumber=" + phoneNumber + "]";
	}



	

}
